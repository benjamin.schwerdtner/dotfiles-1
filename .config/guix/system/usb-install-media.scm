;; With Eshell:
;; guix system -L ~/.config/guix/system disk-image ~/.config/guix/system/usb-install-media.scm

;; For a Guix-like installer with nonfree kernel, prefer:
;;   guix system disk-image nongnu/system/install.scm

(define-module (usb-install-media)
  #:use-module (srfi srfi-1)
  #:use-module (default)
  #:use-module (nongnu packages linux)
  #:use-module (gnu))

(use-service-modules
 desktop                                ; To remove bluetooth.
 ;; To remove GDM:
 xorg)

(use-package-modules bootloaders)

(define-public %usb/services
  (cons*
   (remove
    (lambda (service)
      (member (service-kind service)
              (list bluetooth-service
                    cups-pk-helper-service-type
                    geoclue-service
                    gdm-service-type)))
    %ambrevar/services)))

(operating-system
  (inherit %ambrevar/default-os)
  ;; For some reason USB media can only be booted with GRUB set to "bios" mode.
  ;; It could have been possible to specialize default-operating-system
  ;; depending on (command-line) containing "reconfigure" or "disk-image", but
  ;; that's brittle and less explicit to the user.
  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (target "/dev/sda")))
  (kernel linux)
  (file-systems (cons* (file-system
                         (device (file-system-label "guix"))
                         (mount-point "/")
                         (type "ext4"))
                       (file-system
                         (device "none")
                         (mount-point "/tmp")
                         (type "tmpfs")
                         (check? #f))
                       %base-file-systems))
  (services %usb/services))
