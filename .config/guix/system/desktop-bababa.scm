;; With Eshell:
;; *sudo -E guix system -L ~/.config/guix/system reconfigure ~/.config/guix/system/desktop-bababa.scm
(define-module (desktop-bababa)
  #:use-module (srfi srfi-1)
  #:use-module (default)
  #:use-module (nongnu packages linux)
  #:use-module (guix packages)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (gnu services))

(use-service-modules
 desktop                                ; To remove bluetooth.
 networking                             ; To remove NetworkManager.
 ;; To remove GDM:
 xorg)

(use-package-modules
 linux
 ;; Stumpwm:
 wm)

(define linux-fafafa                    ; TODO: Kernel panic: VFS error: cannot mount rootfs
  (package
    (inherit linux)
    (name "linux-fafafa")
    ;; To build a custom kernel, pass it an alternate "kconfig":
    (native-inputs
     `(("kconfig" ,(local-file "./kernel-fafafa.conf"))
       ,@(alist-delete "kconfig" (package-native-inputs linux))))))

(define-public %bababa/services
  (cons*
   (service connman-service-type)
   (service slim-service-type (slim-configuration
                               (display ":0")
                               (vt "vt7")))
   ;; TODO: Extra service results in black screen and CPU hog.
   ;; (service slim-service-type (slim-configuration
   ;;                             (display ":1")
   ;;                             (vt "vt8")))
   (remove
    (lambda (service)
      (or
       (member (service-kind service)
               (list bluetooth-service
                     cups-pk-helper-service-type
                     geoclue-service
                     gdm-service-type
                     ;; NetworkManager.  The applet is a
                     ;; simple-service and must be removed by
                     ;; matching the type name.
                     network-manager-service-type
                     ;; wpa-supplicant-service-type ; Still needed for networking.
                     modem-manager-service-type
                     usb-modeswitch-service-type))
       (member (service-type-name (service-kind service))
               '(network-manager-applet))))
    %ambrevar/services)))

(define-public %bababa/os
  (operating-system
    (inherit %ambrevar/default-os)
    (host-name "bababa")

    (bootloader (bootloader-configuration
                 (bootloader grub-bootloader)
                 (timeout 1)
                 (target "/dev/sdc")))

    (kernel linux)
    (firmware (append (list amdgpu-firmware)
                      %ambrevar/firmware))
    ;; (initrd-modules (fold (lambda (module l) (delete module l))
    ;;                       (operating-system-initrd-modules %ambrevar/default-os)
    ;;                       '("dm-crypt" "xts" "serpent_generic" "wp512" ;for encrypted root partitions
    ;;                         "hid-apple"
    ;;                         "isci"      ;for SAS controllers like Intel C602
    ;;                         ;; Modules for Linux para-virtualized devices, for use in QEMU guests.
    ;;                         ;; "virtio_pci" "virtio_balloon" "virtio_blk" "virtio_net"
    ;;                         ;; "virtio_console"
    ;;                         "virtio-rng" ; TODO: Why can't it be found?  Probably not needed.
    ;;                         )))

    (file-systems (cons* (file-system
                           (device (file-system-label "guix"))
                           (mount-point "/")
                           (type "btrfs")
                           (options "subvol=rootfs,compress=zstd"))
                         (file-system
                           (device (file-system-label "babadata"))
                           (mount-point "/media/babadata")
                           (type "btrfs")
                           (create-mount-point? #t))
                         (file-system
                           (device (file-system-label "bibidata"))
                           (mount-point "/media/bibidata")
                           (type "btrfs")
                           (create-mount-point? #t))
                         (file-system
                           (mount-point "/tmp")
                           (device "none")
                           (type "tmpfs")
                           (check? #f))
                         %base-file-systems))

    (packages (cons* stumpwm
                     (delete iw (operating-system-packages %ambrevar/default-os))))

    (services %bababa/services)))

%bababa/os
