;; With Eshell:
;; *sudo -E guix system -L ~/.config/guix/system reconfigure ~/.config/guix/system/desktop-bababa-emtec.scm
(define-module (desktop-bababa-emtec)
  #:use-module (default)
  #:use-module (desktop-bababa)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (gnu services))

(use-service-modules
 databases                              ; PostgreSQL
 docker                                 ; REVIEW: Needs 'modprobe overlay'?
 nix
 ;; libvirt:
 virtualization)

(use-package-modules
 databases                              ; For postgis.
 geo                                    ; For postgis.
 ;; Nix
 package-management)

(operating-system
  (inherit %bababa/os)
  (packages (cons* nix
                   postgresql
                   (operating-system-packages %bababa/os)))
  (services (cons*
             (service nix-service-type)
             ;; Warning: This is very heavy:
             (service libvirt-service-type
                      (libvirt-configuration
                       (unix-sock-group "kvm")))
             ;; TODO: Might need 9.6 for Iroha.
             (postgresql-service #:extension-packages (list postgis))
             (service docker-service-type)
             %bababa/services))
  (users (cons* (user-account
                 (name (user-account-name %ambrevar/user))
                 (group (user-account-group %ambrevar/user))
                 (supplementary-groups
                  (cons* "docker"
                         (user-account-supplementary-groups %ambrevar/user)))
                 (home-directory (user-account-home-directory %ambrevar/user)))
                  %base-user-accounts)))
