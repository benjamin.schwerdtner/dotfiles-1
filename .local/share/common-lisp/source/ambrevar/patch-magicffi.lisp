(in-package :magicffi)

(defun path-string (path)
  (uiop:unix-namestring
   (uiop:truename* (if (pathnamep path)
                       path
                       (uiop:parse-native-namestring path)))))

;; Original function uses %truename which fails on wildcards.
(defun magic-file (magic pathspec)
  "Returns a textual description of the contents of the PATHSPEC
argument.  PATHSPEC is a pathname designator.  An error of type
MAGIC-ERROR is signaled on failure."
  (or (foreign-funcall "magic_file"
                       cmagic magic
                       :string (path-string pathspec)
                       :string)
      (magic-error magic)))
