#-asdf3.1 (error "`ambrevar' requires ASDF 3.1")

(defsystem "ambrevar"
  :class :package-inferred-system
  :depends-on ("ambrevar/all")
  :components ((:file "patch-magicffi")
               (:file "patch-legit")))
