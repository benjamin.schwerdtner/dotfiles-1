(in-package :org.shirakumo.legit)

(define-git-wrapper git-diff
  &optional commit blob (paths :--)
  &key (patch :bool) (unified :arg=) raw patch-with-raw minimal patience histogram
  (diff-algorithm (:member :patience :minimal :histogram :myers)) (stat :arg=) numstat
  shortstat (dirstat :arg=) summary patch-with-stat (null (:name z) :flag) name-only
  name-status (submodule :arg=) (color :bool :arg=) (word-diff :arg=)
  (word-diff-regex :arg=) (color-words :arg=) no-renames check
  (ws-error-highlight :arg=) full-index binary (abbrev :arg=) (break-rewrites :arg=)
  (find-renames :arg=) (find-copies :arg=) find-copies-harder irreversible-delete
  (limit-find (:name l) :arg.) (diff-filter :arg=)
  (differing-occurrences (:name S) :upcase :arg.)
  (differing-diffs (:name G) :upcase :arg.)
  pickaxe-all pickaxe-regex (order (:name O) :upcase :arg.)
  (swap (:name R) :upcase :flag) (relative :arg=) text ignore-space-at-eol
  ignore-space-change ignore-all-space ignore-blank-lines (inter-hunk-context :arg=)
  function-context (ext-diff :bool) (textconv :bool) (ignore-submodules :flag :arg=)
  (src-prefix :arg=) (dst-prefix :arg=) no-prefix
  cached)
