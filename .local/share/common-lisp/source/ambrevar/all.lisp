(uiop:define-package ambrevar/all
  (:nicknames #:$)
  (:use #:common-lisp)
  (:shadow #:syntax)
  (:use #:glyphs)               ; TODO: Use?  `syntax' conflicts with `fof'.
  (:import-from #:trivia #:match #:guard)
  ;; TODO: Use alexandria and serapeum?  cmd?  fset?
  (:use #:alexandria #:serapeum)
  (:use #:fof #:fof/mediafile)
  ;; Packages we want available at all times:
  (:import-from #:alexandria)
  (:import-from #:bordeaux-threads)
  (:import-from #:calispel)
  (:import-from #:cl-csv)
  (:import-from #:cl-interpol)
  (:import-from #:cl-json)
  (:import-from #:cl-ppcre)
  (:import-from #:cl-ppcre-unicode)
  (:import-from #:cl-prevalence)
  (:import-from #:clesh)
  (:import-from #:closer-mop)
  (:import-from #:cmd)
  (:import-from #:dexador)
  (:import-from #:fset)
  (:import-from #:hu.dwim.defclass-star)
  (:import-from #:iolib)
  (:import-from #:legit)
  (:import-from #:local-time)
  (:import-from #:log4cl)
  (:import-from #:magicffi)
  (:import-from #:mk-string-metrics)
  (:import-from #:moptilities)
  (:import-from #:osicat)
  (:import-from #:pathname-utils)
  (:import-from #:plump)
  (:import-from #:quicksearch)
  (:import-from #:quri)
  (:import-from #:repl-utilities)
  (:import-from #:serapeum)
  (:import-from #:series)
  (:import-from #:str)
  (:import-from #:supertrace)
  (:import-from #:trivia)
  (:import-from #:trivial-benchmark)
  (:import-from #:trivial-file-size)
  (:import-from #:trivial-package-local-nicknames)
  (:import-from #:trivial-types)
  (:import-from #:unix-opts)
  ;; Top-level packages:
  (:use-reexport
   #:ambrevar/debug
   #:ambrevar/emacs
   #:ambrevar/encfs
   #:ambrevar/guix
   #:ambrevar/shell
   #:ambrevar/storage
   #:ambrevar/syspack)
  ;; No need to `use' this the readtable:
  (:import-from #:ambrevar/syntax))

(in-package ambrevar/all)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (trivial-package-local-nicknames:add-package-local-nickname :alex :alexandria)
  (trivial-package-local-nicknames:add-package-local-nickname :sera :serapeum)
  (trivial-package-local-nicknames:add-package-local-nickname :path :pathname-utils))
