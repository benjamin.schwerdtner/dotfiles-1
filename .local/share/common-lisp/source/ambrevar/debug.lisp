(uiop:define-package ambrevar/debug
  (:documentation "Progamming helpers")
  (:use :common-lisp)
  (:import-from #:serapeum #:export-always)
  (:import-from #:cl-ppcre))
(in-package ambrevar/debug)

;; TODO: Implement SLY function instead.
;; https://github.com/joaotavora/sly/issues/362
(export-always 'trace-all)
(defun trace-all (regexp &optional (package *package*))
  (let ((package (if (packagep package)
                     package
                     (uiop:ensure-package package)))
        (result nil))
    (do-symbols (s package)
      (when (and (eq (symbol-package s) package)
                 (ppcre:scan regexp (string s)))
        (push s result)))
    (eval `(trace ,@result))))
