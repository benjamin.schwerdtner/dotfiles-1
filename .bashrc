# Bash initialization for interactive non-login shells and
# for remote shells (info "(bash) Bash Startup Files").

# Export 'SHELL' to child processes.  Programs such as 'screen'
# honor it and otherwise use /bin/sh.
export SHELL

if [[ $- != *i* ]]
then
    # We are being invoked from a non-interactive shell.  If this
    # is an SSH session (as in "ssh host command"), source
    # /etc/profile so we get PATH and other essential variables.
    [[ -n "$SSH_CLIENT" ]] && source /etc/profile

    # Don't do anything else.
    return
fi

# Source the system-wide file.
source /etc/bashrc

# Adjust the prompt depending on whether we're in 'guix environment'.
if [ -n "$GUIX_ENVIRONMENT" ]
then
		PS1='\e[1;37m[\e[1;32m\w\e[1;37m]\e[m \e[0;34m\D{%F %T}\e[m\n\e[1;37m[env]\$\e[m '
else
		PS1='\e[1;37m[\e[1;32m\w\e[1;37m]\e[m \e[0;34m\D{%F %T}\e[m\n\e[1;37m\$\e[m '
fi

mkcd(){
	mkdir -p "$@" && cd "$1"
}

alias ls='ls -p --color=auto'
alias ll='ls -lh'
alias la='ls -lAh'
alias l='ls -1'
alias grep='grep --color=auto'
