(specifications->manifest
  '("acpi"
    "aircrack-ng"
    "bbswitch-module"                   ; TODO: Should be part of the system.
    "bluez"
    "cryptsetup"
    "emacs-bluetooth"
    "noisetorch"
    "powertop"
    "v4l-utils"
    "xbacklight"))
