#!/bin/sh
## This file should be automatically sourced by the login manager or Bash if
## .bash_profile does not exist.  If this file is not automatically sourced,
## do it from the shell config to me sure it applies to TTY as well.

## Preliminary path definitions.  For security reasons (and bad programming
## assumptions) you should always append entries to PATH, not prepend them.
appendpath () {
	[ $# -eq 2 ] && PATHVAR=$2 || PATHVAR=PATH
	[ -d "$1" ] || return
	eval echo \$$PATHVAR | grep -q "\(:\|^\)$1\(:\|$\)" && return
	eval export $PATHVAR="\$$PATHVAR:$1"
}
prependpath () {
	[ $# -eq 2 ] && PATHVAR=$2 || PATHVAR=PATH
	[ -d "$1" ] || return
	eval echo \$$PATHVAR | grep -q "\(:\|^\)$1\(:\|$\)" && return
	eval export $PATHVAR="$1:\$$PATHVAR"
}

## Custom paths
export PERSONAL="$HOME/projects/personal"

## Cask
appendpath "$HOME/.cask/bin"

## Guix
export GUIX_DISTRO_AGE_WARNING=1m
## WARNING: GUIX_PACKAGE_PATH is deprecated in favor of channels.
# if command -v guix >/dev/null 2>&1; then
# 	export GUIX_PACKAGE_PATH="$HOME/.guix-packages"
# fi
for i in ~/.guix-extra-profiles/*; do
 	profile=$i/$(basename "$i")
 	if [ -f "$profile"/etc/profile ]; then
 		GUIX_PROFILE="$profile" ; . "$profile"/etc/profile
		export MANPATH="$profile"/share/man:$MANPATH
		export INFOPATH="$profile"/share/info:$INFOPATH
		export XDG_DATA_DIRS="$profile"/share:$XDG_DATA_DIRS
		export XDG_CONFIG_DIRS="$profile"/etc/xdg:$XDG_CONFIG_DIRS
 	fi
 	unset profile
done

## progress
if command -v progress >/dev/null 2>&1; then
	emacs_version=$(emacs -Q --batch --eval '(message emacs-version)' 2>&1)
	export PROGRESS_ARGS="-a emacs-$emacs_version -a .emacs-$emacs_version-real"
fi

## Use this to override system executables.
prependpath "$PERSONAL/hackpool"

## Last PATH entries.
appendpath "$HOME/.local/bin"

## Remove less history.
LESSHISTFILE='-'

## Manpage.
export MANWIDTH=80

## SSH-Agent
## Set SSH to use gpg-agent
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi
# Set GPG TTY
export GPG_TTY=$(tty)
# Refresh gpg-agent tty in case user switches into an X session
gpg-connect-agent updatestartuptty /bye >/dev/null

## Linux specific
if [ "$(uname -o)" = "GNU/Linux" ] ; then
	## Startup error log.
	log_dmesg="$(dmesg | grep -i error)"
	[ -n "$log_dmesg" ] && echo "$log_dmesg" > "$HOME/errors-dmesg.log" || rm "$HOME/errors-dmesg.log" 2>/dev/null
	unset log_dmesg
fi

## Wine DLL overrides.
## Remove the annoying messages for Mono and Gecko.
export WINEDLLOVERRIDES="mscoree,mshtml="
## Do not create desktop links or start menu entries.
export WINEDLLOVERRIDES="$WINEDLLOVERRIDES;winemenubuilder.exe=d"

## Default text editor
## 'em' is a custom wrapper for emacsclient. See '.bin/em'.
## VISUAL is given priority by some programs like Mutt. This way we can separate
## editors that wait from those that don't.
for i in emacsclient em emacs zile vim nano vi; do
	command -v $i >/dev/null 2>&1 && export EDITOR=$i && break
done
GIT_EDITOR="$EDITOR"
VISUAL="$EDITOR"
[ "$GIT_EDITOR" = em ] && GIT_EDITOR=emc
[ "$VISUAL" = em ] && VISUAL=emw
export GIT_EDITOR
export VISUAL

## Hook. Should be sourced last
[ -f ~/.profile_hook ] && . ~/.profile_hook
## Hook example
#
# export CPPFLAGS=-I$HOME/local/usr/include
# export LDFLAGS=-L$HOME/local/usr/lib
#
# appendpath "$HOME/local/usr/lib/python2.7/dist-packages/" PYTHONPATH
# export LUA_CPATH="$HOME/local/usr/lib/lib?.so;$(lua -e "print(package.cpath)")"
#
# umask 077

## End: Source .bashrc. The rc file should guard against non-interactive shells.
[ "$(ps -o comm= $$)" != bash ] && return
[ -f ~/.bashrc ] && . ~/.bashrc

[ -z "$DISPLAY" ] && [ "$(tty)" = '/dev/tty1' ] && exec xinit -- vt01
