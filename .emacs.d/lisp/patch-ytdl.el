(defun ytdl--get-default-filename (url)
  "Get default filename from web server.

Query the default-filename of URL using '--get-filename' argument
of ytdl."
  (if (equal ytdl-always-query-default-filename
             'never)
      nil
    (with-temp-buffer
      (apply #'call-process ytdl-command nil t nil
             (append ytdl-download-extra-args
                     (list
                      "--get-filename"
                      "--restrict-filenames"
                      "--" url)))
      (goto-char (point-min))
      (if (search-forward-regexp "^ERROR" nil t)
          (progn
            (beginning-of-line)
            (error (buffer-substring-no-properties (line-beginning-position)
                                                   (line-end-position))))
        (search-forward ".")
        (replace-regexp-in-string "/\\|_"
                                  "-"
                                  (buffer-substring-no-properties (line-beginning-position)
                                                                  (1- (point))))))))

(provide 'patch-ytdl)
