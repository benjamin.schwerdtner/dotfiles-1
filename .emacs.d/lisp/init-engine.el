;;; Engine mode

(engine-mode)
(require 'eww)

(defun ambrevar/engine-eww-function (url &optional _)
  (interactive)
  (eww url))

(defengine arch-aur
  "https://aur.archlinux.org/packages.php?O=0&K=%s&do_Search=Go"
  :keybinding "aur")

(defengine arch-packages
  "https://www.archlinux.org/packages/?sort=&q=%s&maintainer=&flagged="
  :keybinding "archp")

(defengine arch-wiki
  "https://wiki.archlinux.org/index.php?title=Special%%3ASearch&search=%s&go=Go"
  :keybinding "archw"
  :browser 'ambrevar/engine-eww-function)

(defengine cookbook
  "https://en.wikibooks.org/wiki/Special:Search?search=%s&prefix=Cookbook%3A&fulltext=Search+Cookbook&fulltext=Search&ns0=1&ns4=1&ns102=1&ns110=1&ns112=1"
  :keybinding "cb")

(defengine ctan
  "https://www.ctan.org/search?phrase=%s"
  :keybinding "ctan")

(defengine devdocs
  "https://devdocs.io/#q=%s"
  :keybinding "dd")

(defengine emacs-debbugs
  "https://debbugs.gnu.org/cgi/pkgreport.cgi?package=emacs;include=subject%3A%s;repeatmerged=on;archive=both"
  :keybinding "emacsb"
  :browser 'ambrevar/engine-eww-function)

(defengine emacs-devel
  "https://lists.gnu.org/archive/cgi-bin/namazu.cgi?idxname=emacs-devel&submit=Search&query=%s"
  :keybinding "emacsd"
  :browser 'ambrevar/engine-eww-function)

(defengine gentoo
  "https://wiki.gentoo.org/index.php?title=Special%3ASearch&search=%s&go=Go"
  :keybinding "gentoo")

(defengine github
  "https://github.com/search?ref=simplesearch&q=%s"
  :keybinding "gh")

(defengine goodreads
  "https://www.goodreads.com/search?q=%s"
  :keybinding "gr")

(defengine guix-devel
  "https://lists.gnu.org/archive/cgi-bin/namazu.cgi?idxname=guix-devel&submit=Search&query=%s"
  :keybinding "guixd"
  :browser 'ambrevar/engine-eww-function)

(defengine guix-help
  "https://lists.gnu.org/archive/cgi-bin/namazu.cgi?idxname=help-guix&submit=Search!&query=%s"
  :keybinding "guixh"
  :browser 'ambrevar/engine-eww-function)

(defengine guix-issues
  "https://issues.guix.info/search?query=%s"
  :keybinding "guixi")

(defengine guix-packages
  "http://data.guix.gnu.org/repository/1/branch/master/package/%s"
  :keybinding "guixp")

(defengine imdb
  "http://www.imdb.com/find?q=%s&s=all"
  :keybinding "imdb")

(defengine musicbrainz-artists
  "https://musicbrainz.org/search?query=%s&type=artist&method=indexed"
  :keybinding "mba")

(defengine musicbrainz-releases
  "https://musicbrainz.org/search?query=%s&type=release&method=indexed"
  :keybinding "mbr")

(defengine nyxt
  "https://github.com/atlas-engineer/nyxt/issues?q=%s"
  :keybinding "nyxt")

(defengine openstreetmap
  "https://www.openstreetmap.org/search?query=%s"
  :keybinding "osm")

(defengine stackoverflow
  "https://stackoverflow.com/search?q=%s"
  :keybinding "so")

(defengine wikipedia
  "https://www.wikipedia.org/search-redirect.php?language=en&go=Go&search=%s"
  :keybinding "wp"
  :docstring "Search Wikipedia!")

(defengine wikibooks
  "https://en.wikibooks.org/wiki/Special:Search?search=%s"
  :keybinding "wb")

(defengine wiktionary
  "https://en.wiktionary.org/wiki/Special:Search?search=%s"
  :keybinding "wk")

(defengine wine-appdb
  "https://www.winehq.org/search/?q=%s"
  :keybinding "wine")

(load (expand-file-name "bookmarks/engines.el" (getenv "PERSONAL")) t)

(provide 'init-engine)
