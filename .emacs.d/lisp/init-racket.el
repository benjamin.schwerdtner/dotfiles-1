;;; Racket

(require 'init-lispy)

(add-hook 'racket-mode-hook 'ambrevar/init-lispy)
(add-hook 'racket-repl-mode-hook 'ambrevar/init-lispy)

(when (fboundp 'rainbow-delimiters-mode)
  (add-hook 'racket-mode-hook #'rainbow-delimiters-mode)
  (add-hook 'racket-repl-mode-hook #'rainbow-delimiters-mode))


(provide 'init-racket)
