
(defun ambrevar/slime-rainbow-init ()
  (font-lock-mode -1)
  (rainbow-delimiters-mode)
  (font-lock-mode))

;; REVIEW: Fix issue https://github.com/slime/slime/issues/523:
;; Remove with SLIME 2.25.
(setq slime-defpackage-regexp
      "^(\\(cl:\\|common-lisp:\\|uiop:\\)?\\(defpackage\\|define-package\\)\\>[ \t']*")

(setq slime-lisp-implementations
      '((sbcl ("sbcl" "--noinform"))
        (ccl ("ccl"))
        (ecl ("ecl"))))
(let ((slime-extra '(slime-fancy
                     ;; slime-banner
                     slime-mrepl
                     slime-xref-browser
                     ;; slime-highlight-edits ; A bit slow...
                     slime-sprof
                     slime-quicklisp
                     slime-asdf
                     slime-indentation)))
  ;; TODO: Fix slime-repl-ansi-color.
  ;; (when (require 'slime-repl-ansi-color nil t)
  ;;   (add-to-list 'slime-extra 'slime-repl-ansi-color)
  ;;   (setq slime-repl-ansi-color t))

  ;; slime-company should not be `require'd, see
  ;; https://github.com/anwyn/slime-company/issues/11.
  (when (ignore-errors (find-library-name "slime-company"))
    (add-to-list 'slime-extra 'slime-company))
  (define-key slime-editing-map (kbd "C-c C-d C-h") 'slime-documentation-lookup)
  (slime-setup slime-extra)
  (add-hook 'slime-repl-mode-hook 'ambrevar/init-lispy)
  (add-hook 'slime-repl-mode-hook #'ambrevar/slime-rainbow-init))

(when (require 'helm-slime nil :noerror)
  (with-eval-after-load 'slime-repl
    (defun ambrevar/helm/slime-set-keys ()
      (define-key slime-repl-mode-map (kbd "M-p") 'helm-slime-repl-history)
      (define-key slime-repl-mode-map (kbd "M-s") nil)
      (define-key slime-repl-mode-map (kbd "M-s f") 'helm-comint-prompts-all)
      (define-key slime-autodoc-mode-map (kbd "C-c C-d C-a") 'helm-slime-apropos)
      (define-key slime-repl-mode-map (kbd "C-c C-x c") 'helm-slime-list-connections)
      ;; REVIEW: Seems that helm-company is less useful than helm-slime-complete.
      (define-key slime-mode-map (kbd "M-<tab>") 'helm-slime-complete)
      (dolist (key '("M-<tab>" "<tab>"))
        (define-key slime-repl-mode-map (kbd key) 'helm-slime-complete)))
    (add-hook 'slime-repl-mode-hook 'ambrevar/helm/slime-set-keys))
  (global-helm-slime-mode)
  (add-to-list 'helm-source-names-using-follow "SLIME xrefs"))

(provide 'init-slime)
