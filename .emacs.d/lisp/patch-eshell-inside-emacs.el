;;; Reported upstream, see #39596.
(defun ambrevar/set-inside-emacs ()
  "Export INSIDE_EMACS just like M-x shell does.
This is useful for programs like Guix that take provisions for Emacs."
  (setenv "INSIDE_EMACS" (format "%s;%s" emacs-version "eshell")))

(add-hook 'eshell-mode-hook 'ambrevar/set-inside-emacs)

(provide 'patch-eshell-inside-emacs)
