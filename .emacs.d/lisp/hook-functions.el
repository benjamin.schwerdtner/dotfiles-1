;;; TODO: Replace useless individual comments with a single global comment.

(defun ambrevar/turn-on-column-number-mode ()
  "Unconditionally turn on `column-number-mode' for the current buffer."
  (set (make-variable-buffer-local 'column-number-mode) t))

(defun ambrevar/turn-on-complete-filename ()
  "Unconditionally turn on `comint-dynamic-complete-filename' for the current buffer."
  (add-to-list 'completion-at-point-functions 'comint-dynamic-complete-filename t))

(defun ambrevar/turn-off-indent-tabs ()
  "Unconditionally turn off tab indentation."
  (setq indent-tabs-mode nil))

(defun ambrevar/turn-on-indent-tabs ()
  "Unconditionally turn on tab indentation."
  (setq indent-tabs-mode t))

(defun ambrevar/turn-off-line-number-mode ()
  "Unconditionally turn off `line-number-mode' fur the current buffer.."
  (set (make-variable-buffer-local 'line-number-mode) nil))

(defun ambrevar/turn-on-newline-paragraph ()
  "Unconditionally make of newlines the start of a paragraph."
  (set (make-local-variable 'paragraph-start) "
"))

(defun ambrevar/turn-off-nobreak-char-display ()
  (set (make-local-variable 'nobreak-char-display) nil))

(defun ambrevar/turn-on-skeleton-markers ()
  "Allow skeletons to make markers to ease field navigation."
  (require 'patch-skeletons)
  (add-hook 'skeleton-end-hook 'ambrevar/skeleton-make-markers))

(defun ambrevar/turn-on-tab-width-to-4 ()
  "Unconditionally set tab width to 4."
  (setq tab-width 4))

(defun ambrevar/turn-on-tab-width-to-8 ()
  "Unconditionally set tab width to 8."
  (setq tab-width 8))

(provide 'hook-functions)
