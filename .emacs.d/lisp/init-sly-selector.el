(require 'sly-mrepl)                    ; For `sly-mrepl--find-buffer'.

(defvar ambrevar/sly-connection-name "sbcl-ambrevar")

(defun ambrevar/helm-sly-buffer-p (buffer)
  "Return non-nil if BUFFER has a SLY connection matching
`ambrevar/sly-connection-name'."
  (with-current-buffer buffer
    (and (derived-mode-p 'sly-mrepl-mode)
         sly-buffer-connection
         (sly-process sly-buffer-connection)
         (string-prefix-p ambrevar/sly-connection-name
                          (sly-connection-name sly-buffer-connection)))))

(defun ambrevar/helm-sly-buffer-non-ambrevar-p (buffer)
  "Return non-nil if BUFFER has a SLY connection not matching
`ambrevar/sly-connection-name'."
  (with-current-buffer buffer
    (and (derived-mode-p 'sly-mrepl-mode)
         sly-buffer-connection
         (sly-process sly-buffer-connection)
         (not (string-prefix-p ambrevar/sly-connection-name
                               (sly-connection-name sly-buffer-connection))))))

(defun ambrevar/helm-sly-mini-sources (name predicate)
  (list (helm-sly--c-source-connection
         (helm-sly--repl-buffer-candidates
          nil predicate)
         name)
        helm-sly-new
        (helm-sly-lisp-buffer-source)
        (helm-sly-build-buffers-source)))

(defun ambrevar/helm-sly-mini ()
  "Helm for Lisp connections and buffers using the
`ambrevar/sly-connection-name' connection."
  (interactive)
  (helm :sources (ambrevar/helm-sly-mini-sources
                  "Ambrevar's Lisp connections"
                  #'ambrevar/helm-sly-buffer-p)
        :buffer "*ambrevar/helm-sly-mini*"))

(defun ambrevar/helm-sly-mini-non-ambrevar ()
  "Helm for Lisp connections and buffers not using the
`ambrevar/sly-connection-name' connection."
  (interactive)
  (helm :sources (ambrevar/helm-sly-mini-sources
                  "Other Lisp connections"
                  #'ambrevar/helm-sly-buffer-non-ambrevar-p)
        :buffer "*ambrevar/helm-sly-mini*"))

(defun ambrevar/helm-selector-sly ()
  "Helm for `sly' buffers using the `ambrevar/sly-connection-name' connection."
  (interactive)
  (helm-selector
   "Ambrevar SLY-REPL"
   :predicate #'ambrevar/helm-sly-buffer-p
   :make-buffer-fn (lambda ()
                     (interactive)
                     (let ((sly-lisp-implementations
                            (list
                             (assoc (intern ambrevar/sly-connection-name)
                                    sly-lisp-implementations)))
                           (current-connection (car (sly--purge-connections))))
                       (if (and current-connection
                                (sly-mrepl--find-buffer current-connection)
                                (ambrevar/helm-sly-buffer-p
                                 (sly-mrepl--find-buffer current-connection)))
                           (call-interactively #'sly)
                         (sly))))
   :helm-sources #'ambrevar/helm-sly-mini))

(defun ambrevar/helm-selector-sly-other-window ()
  "Like `ambrevar/helm-selector-sly' but raise buffer in other window."
  (interactive)
  (let ((current-prefix-arg t))
    (call-interactively #'helm-selector-sly)))

(defun ambrevar/helm-selector-sly-non-ambrevar ()
  "Helm for `sly' buffers not using the `ambrevar/sly-connection-name' connection."
  (interactive)
  (helm-selector
   "SLY-REPL for all but Ambrevar's shells."
   :predicate #'ambrevar/helm-sly-buffer-non-ambrevar-p
   :make-buffer-fn (lambda ()           ; Copied from helm-selector-sly.el.
                     (interactive)
                     (let ((current-connection (car (sly--purge-connections))))
                       (if (and current-connection
                                (sly-mrepl--find-buffer current-connection)
                                (ambrevar/helm-sly-buffer-non-ambrevar-p
                                 (sly-mrepl--find-buffer current-connection)))
                           ;; Make sure to call interactively so that last
                           ;; connection is reused.
                           (call-interactively #'sly)
                         (let ((current-prefix-arg '-))
                           (call-interactively #'sly)))))
   :helm-sources #'ambrevar/helm-sly-mini-non-ambrevar))

(defun ambrevar/helm-selector-sly-non-ambrevar-other-window ()
  "Like `ambrevar/helm-selector-sly-non-ambrevar' but raise buffer in other window."
  (interactive)
  (let ((current-prefix-arg t))
    (call-interactively #'helm-selector-sly)))

(provide 'init-sly-selector)
